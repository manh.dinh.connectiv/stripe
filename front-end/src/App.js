import logo from './logo.svg';
import './App.css';
import axios from 'axios';

function App() {
  const PRICE = process.env.REACT_APP_PRICE;

  const orderPlan = (price) => {
    axios({
      method:'post',
      url:'http://127.0.0.1:8000/api/payment',
      data:{
        price: price,
        email: 'manhdinh@gamil.com'
      }
    })
      .then(function(res) {
        console.log('manh', res.data)
        const { data: checkoutSession } = res
        window.location.href = checkoutSession.url
      })
  }
  

  return (
    <div className="App">
      <header className="App-header">
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <button onClick={() => orderPlan(PRICE)}>Buy now</button>
      </header>
    </div>
  );
}

export default App;

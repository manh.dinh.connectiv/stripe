<?php

return [

    /*
    |--------------------------------------------------------------------------
    | STRIPE SDK Configuration
    |--------------------------------------------------------------------------

    | Manage your API keys to authenticate requests with Stripe.
    | https://stripe.com/docs/keys
    |
    */
    'publishable_key'       => env('STRIPE_PUBLISHABLE_KEY', ''),
    'secret_key'            => env('STRIPE_SECRET_KEY', ''),
    'version'               => 'latest',
];

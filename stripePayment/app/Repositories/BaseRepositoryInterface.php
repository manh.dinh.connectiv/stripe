<?php
namespace App\Repositories;

interface BaseRepositoryInterface
{
    /**
     * Display a listing of the resource.
     *
     * @return mixed
     */
    public function all();

    /**
     * Get the specified resource from storage.
     *
     * @param $id
     * @return mixed
     */
    public function find($id);

    /**
     * Store a newly created resource in storage.
     *
     * @param array $attributes
     * @return mixed
     *
     */
    public function create(array $attributes);

    /**
     * Update the specified resource in storage.
     *
     * @param $id
     * @param array $attributes
     * @return mixed
     */
    public function update($id, array $attributes);

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return mixed
     */
    public function delete($id);
}

<?php

namespace App\Repositories;

use App\Repositories\BaseRepositoryInterface;
use Illuminate\Database\Eloquent\Model;

class BaseRepository implements BaseRepositoryInterface
{
    /**
     * @var \Illuminate\Database\Eloquent\Model
     */
    protected $model;

    /**
     * BaseRepository constructor.
     * @param Model $model
     */
    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Model
     */
    public function all()
    {
        return $this->model;
    }

    /**
     * Get the specified resource from storage.
     *
     * @param $id
     * @return Model $model
     */
    public function find($id)
    {
        return $this->model->findOrFail($id);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param array $attributes
     * @return mixed
     */
    public function create($attributes = [])
    {
        return $this->model->create($attributes);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param int $id
     * @param array $attributes
     * @return bool
     */
    public function update($id, $attributes = [])
    {
        return $this->find($id)->update($attributes);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return bool
     * @throws \Exception
     */
    public function delete($id)
    {
        return $this->find($id)->delete();
    }
}

<?php

namespace App\Repositories\User;

use App\Repositories\BaseRepository;
use App\Models\User;
use App\Services\Stripe\StripeService;
use Illuminate\Database\Eloquent\Model;

class OderRepository extends  BaseRepository implements OderRepositoryInterface
{
    protected $stripe;

    public function __construct(User $model, StripeService $stripe)
    {
        parent::__construct($model);
        $this->stripe = $stripe;
    }
    public function store(array $attribute)
    {
        $checkoutStripe = $this->stripe->checkout($attribute['price'], $attribute['email']);

//        if ($checkoutStripe) {
//            // TODO update plan after checkout
//        }
        return $checkoutStripe;
    }
}

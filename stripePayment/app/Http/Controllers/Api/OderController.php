<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\User\OderRepositoryInterface;

class OderController extends Controller
{
    protected $repository;

    /**
     * @param OderRepositoryInterface $oderRepository
     */
    public function  __construct(OderRepositoryInterface $oderRepository)
    {
        $this->repository = $oderRepository;
    }

    public function oderPlan(Request $request)
    {
        //dd($request->all());
        return $this->repository->store($request->all());
    }
}

<?php

namespace App\Services\Stripe;

use \Stripe\StripeClient;
use Illuminate\Support\Facades\Log;

class StripeService
{
    protected $client;

    public function __construct(StripeClient $client)
    {
        $this->client = new $client(config('stripe.secret_key'));
    }

    /**
     * https://stripe.com/docs/api/checkout/sessions/create
     */
    public function checkout($price, $email)
    {
        $checkoutSession = $this->client->checkout->sessions->create([
            'success_url' => 'http://localhost:3000/',
            'cancel_url' => 'http://localhost:3000/',
            'payment_method_types' => ['card'],
            'customer_email' => $email,
            'line_items' => [
                [
                    'price' => $price,
                    'quantity' => 1,
                ],
            ],
            'mode' => 'subscription',
        ]);
        Log::debug($checkoutSession);
        return $checkoutSession;
    }
}

